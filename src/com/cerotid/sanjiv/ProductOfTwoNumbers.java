package com.cerotid.sanjiv;

import java.util.Scanner;
public class ProductOfTwoNumbers {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter the first Number: ");
		int a = myScanner.nextInt();
		
		System.out.println("Enter the second Number: ");
		int b = myScanner.nextInt();
		
		System.out.println( a + " X " + b + " = " +  a *  b);
		

	}

}
