package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer {
	private String firstName;
	private String lastName;
	private ArrayList<Account> accounts;
	private String address;

	public Customer() {

	}

	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;

	}

	public ArrayList<Account> getAccounts() {
		return accounts;

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	void printCustomerDetails() {

		System.out.println(toString());

	}

	@Override
	public String toString() {
		return "Customer[firstName=" + firstName + ", lastName=" + lastName + ", accounts=" + accounts + ", address="
				+ address + "]";

	}

	public static Comparator<Customer> LastNameComparator = new Comparator<Customer>() {
		pulic int compare(Customer o1, Customer o2) {
			if(01.getLastName().compareTo(o2.getLastName()) == 0) {
				return o1.getFirstName().compareTo(o2.getFirstName());
				
			}
			return o1.getLastName().compareTo(o2.getlastName()));
			
		}
		
		
	}

}

}
