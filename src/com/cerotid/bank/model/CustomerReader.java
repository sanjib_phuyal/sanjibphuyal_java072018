package com.cerotid.bank.model;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.util.ArrayList;

public class CustomerReader {
	private ArrayList<Customer> customerList = new ArrayList<>();
	
	public void readFile() throws IOException {
	File file = file File("CustomerInput.txt");
	
	FileReader fileReader = new FileReader(file);
	BufferedReader bufferedReader = new BufferedReader(fileReader);
	
	String line;
	
	while ((line = bufferedReader.readLine()) != null) {
		ArrayList<String> list = new ArrayList<String>(Arrays.aslist(line.split(",")));
		
		String firstName = list.get(1);
		String lastName = list.get(0);
		
		Customer customer = new Customer(firstName, lastName);
		
		customerList.add(customer);
		
	}
	
	bufferedReader.close();
	fileReader.close();
	System.out.println("Contents of file");
	

	}

	
	
	public void writeFile() {
		File file = new File("CustomerSortedOutput.txt");
		
		FileWriter fileWriter = new FileWriter(file);
		sortCustomersWithTheirLastName();
		
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		
		for (Customer cust: customerList) {
			bufferedWriter.write(cust.getFirstName());
			bufferedWrite.write(" ");
			bufferedWrite.write(cust.getLastName());
			bufferedWriter.write("\n");
			
			
		}
		buffredWriter.close();
		fileWriter.close();
		
		
		}
	private void sortCustomersWithTheirLastName() {
		Collections.sort(customerList,Customer.LastNameComparator);
		
		for (Customer cust: customerList) {
			System.out.println(cust.getLastName());
			
		}
	}
	
}
